<?php
/**
 *
 * EXCEPTION theme Header
 * @version 1.0.0
 *
 */ 
 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo theme_option('shape'); ?>" data-class="<?php echo theme_option('shape'); ?>">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />     
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<?php echo esc_url(get_stylesheet_uri()); ?>" type="text/css" media="screen" />
		<?php if ( ! isset( $content_width ) ) $content_width = 960; ?>
		<?php it_title_css(); ?>
		<?php wp_head(); ?>
		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop Google Tag Manager head code here
			| Hot Jar tracking will be added within Google Tag Manager
			|
			\*----------------------------------------------------------------*/
			?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-37739718-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-37739718-1');
			</script>
		<?php } ?>
		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"marketing\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop any marketing tracking pixels here
			| These are related to paid advertising like Google Ads
			| and Facebook ad pixels.
			|
			\*----------------------------------------------------------------*/
			?>
		
		<?php } ?>
	</head>
	<body <?php body_class(); ?>>

		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop Google Tag Manager body code here
			|
			\*----------------------------------------------------------------*/
			?>
		<?php } ?>
        
		<?php
		
		// Enable / Disable Smooth scroll.
		$anims = $an_in = $an_out = $anim_cov = '';
		$anim_in = theme_option('data-animsition-in');
		$anim_out = theme_option('data-animsition-out');
		if ( theme_option('page_transitions') == "1" ) {
				$anims = 'animsition';
				$an_in = ' data-animsition-in-class="'.$anim_in.'"';
				$an_out = ' data-animsition-out-class="'.$anim_out.'"'; 
		}
		if ( $anim_in == "overlay-slide-in-top" || $anim_in == "overlay-slide-in-bottom" || $anim_in == "overlay-slide-in-left" || $anim_in == "overlay-slide-in-top" ) {
				$anim_cov = 'data-animsition-overlay="true"';
		}
		
		?>
		
		<div class="pageWrapper <?php echo $anims; ?> <?php echo theme_option('layout'); ?>" <?php echo $an_in; ?> <?php echo $an_out; ?> <?php echo $anim_cov; ?>>
		<?php it_theme_header(); ?>
		<div id="contentWrapper">
