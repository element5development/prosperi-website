<?php
/**
 *
 * EXCEPTION theme Header
 * @version 1.0.0
 *
 */ 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo theme_option('shape'); ?>" data-class="<?php echo theme_option('shape'); ?>">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />        
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<?php echo esc_url(get_stylesheet_uri()); ?>" type="text/css" media="screen" />
		<?php if ( ! isset( $content_width ) ) $content_width = 960; ?>
		<?php it_title_css(); ?>
		<?php wp_head(); ?> 
		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop Google Tag Manager head code here
			| Hot Jar tracking will be added within Google Tag Manager
			|
			\*----------------------------------------------------------------*/
			?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-37739718-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-37739718-1');
			</script>
		<?php } ?>
		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"marketing\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop any marketing tracking pixels here
			| These are related to paid advertising like Google Ads
			| and Facebook ad pixels.
			|
			\*----------------------------------------------------------------*/
			?>
		
		<?php } ?>
	</head>
	<body <?php body_class('one-page'); ?>>

		<?php if(isset($_COOKIE['cookieControlPrefs']) && $_COOKIE['cookieControlPrefs'] == '[\"analytics\"]' || $_COOKIE['cookieControlPrefs'] == '[\"analytics\",\"marketing\"]' ) { ?>
			<?php
			/*----------------------------------------------------------------*\
			|
			|	Drop Google Tag Manager body code here
			|
			\*----------------------------------------------------------------*/
			?>
		<?php } ?>

		<?php if ( theme_option('page-loader') ) : ?>
			<!-- site preloader start -->
			<div class="page-loader"></div>
			<!-- site preloader end -->
		<?php endif; ?>
		<div class="pageWrapper <?php echo theme_option('layout'); ?>">
		<?php it_theme_header(); ?>
		<div id="contentWrapper">
