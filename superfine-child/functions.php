<?php
/**
 *
 * IT-RAYS Framework
 *
 * @author IT-RAYS
 * @license Commercial License
 * @link http://www.it-rays.com
 * @copyright 2014 IT-RAYS Themes
 * @package IT-RAYS-Framework
 * @version 1.0.0
 *
 */
locate_template( 'it-framework/init.php', true );

define( 'CHILD_DIR', get_stylesheet_directory() );