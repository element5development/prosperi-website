jQuery(document).ready(function () {
	/*----------------------------------------------------------------*\
		COOKIE CONSENT
		Note: Accepted tracking cookies should be enabled in Google Tag Manager
		https://www.grahamomaonaigh.com/gdpr-compliant-cookies-google-analytics/
	\*----------------------------------------------------------------*/
	// Available options located at https://github.com/ketanmistry/ihavecookies
	var options = {
    title: 'Cookies & Privacy',
		message: 'Cookies enable you to personalize your experience on our site, tell us which parts of our website people have visited, help us measure the effectiveness of ads and web searches, and give us insights into user behavior so we can improve our communications and products.',
		link: '/privacy-policy/',
		delay: 0,
		expires: 30, // 30 days
		moreInfoLabel: 'More information',
		acceptBtnLabel: 'Accept Cookies',
		advancedBtnLabel: 'Customize',
		cookieTypesTitle: 'Select cookies to accept',
		uncheckBoxes: false,
		// Optional callback function when 'Accept' button is clicked
		onAccept: function() {
			var myPreferences=jQuery.fn.ihavecookies.cookie();
			console.log('The following cookie preferences have been saved:');
			console.log(myPreferences);
		},
		// Array of cookie types for which to show checkboxes.
		// - type: Type of cookie. This is also the label that is displayed.
		// - value: Value of the checkbox so it can be easily identified in your application.
		// - description: Description for this cookie type. Displayed in title attribute.
		cookieTypes: [
			{
				type: 'Analytics',
				value: 'analytics',
				description: 'Cookies related to site visits, browser types, etc.'
			},
			{
				type: 'Marketing',
				value: 'marketing',
				description: 'Cookies related to marketing, e.g. newsletters, social media, etc'
			}
		],
	}
	jQuery('body').ihavecookies(options);
	// Reopen cookie consent
	jQuery('button.cookie-open').click(function() {
    jQuery('body').ihavecookies(options, 'reinit');
	});
	// Check if preference is selected
	if (jQuery.fn.ihavecookies.preference('analytics') === true) {
		console.log('Analytics cookies have been accepted.');
	}
	if (jQuery.fn.ihavecookies.preference('marketing') === true) {
		console.log('Marketing cookies have been accepted.');
	}
});