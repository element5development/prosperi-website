<div class="page-title title-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 lft-title center">
                <div class="title-container">
                    <h1 class="main-bg fx shape" data-animate="fadeInUp"><?php echo it_custom_page_title(); ?></h1>
                    <h3 class="fx sub-title" data-animate="fadeInDown"><?php echo esc_html__('Profile page','superfine') ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
